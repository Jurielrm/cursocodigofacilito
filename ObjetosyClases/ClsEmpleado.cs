﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetosyClases
{
    //"Public" se le agrega a la clase para que sea publica 
    public class ClsEmpleado
        /*CLASE: Descripcion de un grupo de objetos con prorpiedades (atributos), 
          comportamiento (operaciones) relaciones y semanticas comunes*/
    {
        /*Caracteristicas del CONSTRUCTOR:
         *Es publico, 
         *No regresa ningun tipo 
         *Y tiene el mismo nombre que la clase
         *Inicializa los valores de las Propiedades */
        public ClsEmpleado()
        {
            Nombre = "";
            SueldoDiario = 0.0m;
            Edad = 0;
        }
        //PROPIEDADES
        public string Nombre;
        public decimal SueldoDiario;
        public int Edad;
        //Metodo que regresa un valor decimal y (Recibe un parametro entero)
        public decimal CalculaSalario (int NumeroDias)
        {
            return SueldoDiario * NumeroDias;//Regresa al SueldoDiario y lo * NumeroDias
        }
    }
}
