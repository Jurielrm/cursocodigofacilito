﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjetosyClases
{
    class Program
    {
        static void Main(string[] args)
        {
            ClsEmpleado empleado;//Declaro una variable empleado de la clase ClsEmpleado
            empleado = new ClsEmpleado();//Instanciamos la clase empleado con new y despues el nombre del "CONSTRUCTOR"
            //En este apartado ya podemos utilizar el objeto empleado y los //En este apartado ya podemos utilizar el objeto empleado y las propiedades que tiene el CONSTRUCTOR
            empleado.Edad = 25;
            empleado.Nombre = "Jaime Uriel";
            empleado.SueldoDiario = 12.5m;
            decimal total;//Se crea una variable Decimal llamada total
            total = empleado.CalculaSalario(30);//Obtiene el calculodelsalario x 30 dias
            Console.WriteLine("El salario mensual del empleado " + empleado.Nombre);//Concatenamos el mensaje con el campo Nombre
            Console.WriteLine("es: " + total.ToString("C"));//"C"la ponemmos para poner lo en formato moneda
            Console.ReadKey();
        }
    }
}
