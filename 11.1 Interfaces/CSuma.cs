﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11._1_Interfaces
{
    class CSuma : IOperacion
    {
        private double r = 0;
        private ArrayList resultados = new ArrayList();
        //Metodo implemantar
        public void calcular(double a, double b)//El metodo debe tener los mismos parametros de IOperacion
        {
            r = a + b;//Implementamos el codigo
        }

        public void mostrar()
        {
            Console.WriteLine("El resultado de la suma es {0}", r);
            Console.ReadKey();//Detenemos la pantalla
            resultados.Add(r);
        }

        //Metodo propios de la clase
        public void muestraResultado()
        {
            foreach (double r in resultados)//Busca en el ArrayList
                Console.WriteLine(r);//Los imprime cada uno de ellos

        }
    }
}
