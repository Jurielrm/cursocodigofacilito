﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11._1_Interfaces
{
    interface IOperacion
    {
        //Comportamiento que mostrara 2 parametros
        void calcular(double a, double b);
        void mostrar();//Mostrara el resultado
    }
}
