﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11._1_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            //OPCION 1 
            //Podemos mostrar la suma y resta de esta forma
            /*CSuma s = new CSuma();
            s.calcular(4, 7);
            s.mostrar();

            CResta r = new CResta();
            r.calcular(5, 6);
            r.mostrar();*/

            //OPCION 2

            int Opcion = 0;
            double ValorA = 0.0;
            double ValorB = 0.0;

            string mensaje = "";
            /*Creamos una variable "operacion" que es de tipo IOperacaion.
              Entonces "operacion" puede actuar como cualquier clase que implemente como herencia 
              la "Interfaz" IOperacion*/
            IOperacion operacion = new CSuma();
            //CICLO WHILE
            while(Opcion!=5)
            {
                Console.WriteLine("1.Suma, 2.Resta, 3. Multiplicacion, 4.Division, 5.Salir");//Muestro mi menu
                Console.WriteLine("¿Que opcion desea utilizar?");//Pregunta que opcion elejir
                mensaje = Console.ReadLine();
                Opcion = Convert.ToInt32(mensaje);//Pido la opcion y convierto el mensaje en int

                Console.WriteLine("Ingrese un numero");
                mensaje = Console.ReadLine();
                ValorA = Convert.ToInt32(mensaje);

                Console.WriteLine("Ingrese un segundo numero");
                mensaje = Console.ReadLine();
                ValorB = Convert.ToInt32(mensaje);

                //POLIMORFISMO
                if (Opcion == 1)//Si toma como Opcion 1
                    operacion = new CSuma();//operacion va actuar como CSuma
                if (Opcion == 2)
                    operacion = new CResta();
                if (Opcion == 3)
                    operacion = new CMultiplicacion();
                if (Opcion == 4)
                    operacion = new CDivision();

                /*Aqui nuestro programa trabaja en terminos del concepto operacion
                 en lugar de en terminos de cosas concretas como suma,resta,multi,div*/
                operacion.calcular(ValorA, ValorB);
                operacion.mostrar();
            }

        }
    }
}
