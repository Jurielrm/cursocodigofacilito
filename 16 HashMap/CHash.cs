﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_HashMap
{
    public class CHash
    {
        Hashtable tabla;
        public CHash()//Constructor
        {
            tabla = new Hashtable();//Instanciamos
        }

        public void Agrega()
        {
            //Agrega 
            tabla.Add("Regaeton", "Genero");
            tabla.Add("Wisin y Yandel", "Artista");
            tabla.Add("Lideres", "Album");
            tabla.Add("Te Deseo", "Nombre");
        }

        public void Imprime()
        {
            Console.WriteLine("Existen {0} elementos, dame algun caracterisitca de la canciona: ", tabla.Count);
            string cadena = Console.ReadLine();
            if (tabla[cadena] == null)
            {
                Console.WriteLine("El elemento no existe");
            }
            else
            {
                Console.WriteLine("El elemento encontrado es: ");
                Console.WriteLine(tabla[cadena]);//Imprime el elemento tabla
            }
           
            Console.ReadKey();
        }

        public void Eliminar(string key)
        {
            tabla.Remove(key);//Elimina el elemento
        }
    }
}
