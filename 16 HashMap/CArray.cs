﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_HashMap
{
    public class CArray
    {
        ArrayList lista;
        public CArray()
        {
            lista = new ArrayList();
        }

        public void Captura()
        {
            string cadena;
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Ingresa el dato {0}: ", i + 1);
                cadena = Console.ReadLine();
                lista.Add(cadena);
            }
            //Se agregan despues los datos
            lista.Add(1);
            lista.Add(2);
        }

        public void Imprime()
        {
            foreach (var item in lista)
            {
                Console.WriteLine(lista);
            }
            Console.ReadKey();
        }
    }
}
