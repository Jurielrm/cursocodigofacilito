﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_Linq_2._2
{
    public abstract class CAbs
    {
        public abstract int IdProducto { get; set; }

        public abstract string Clave { get; set; }

        public abstract string Descripcion { get; set; }
    }
}
