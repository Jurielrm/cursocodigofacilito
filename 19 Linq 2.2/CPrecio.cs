﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_Linq_2._2
{
    public class CPrecio : CBase
    {
        public decimal PrecioPublico { get; set; }

        public decimal Costo { get; set; }
    }
}
