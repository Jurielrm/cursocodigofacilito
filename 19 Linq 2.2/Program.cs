﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _19_Linq_2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            //"Stopwatch" Nos permite iniciar y parar un reloj
            Stopwatch reloj = new Stopwatch();
            Presentacion control = new Presentacion();//Instanciamos
            control.LlenarLista();
            CImpresion.Imprime(control.Productos);//Imprime la lista "Productos"
            CImpresion.Espera();
            
            List<CInventario> lista = new List<CInventario>();//Se crea una nueva lista 
            reloj.Start();//Inicia
            foreach (var item in control.Productos)//Recorre la lista Productos
            {
                if (item.Existencias > 0 && item.PrecioPublico < 12 )//La existencia es mayor que "0" y "PrecioPublico" sea menor que 12 
                    lista.Add(item);//Se agregan a la lista todos los que tengan Existencia menor a "0"
            }
            reloj.Stop();//Detiene
            CImpresion.Imprime(lista);//Imprime la nueva "lista"
            CImpresion.ImprimeTiempo(reloj.ElapsedTicks);//Imprime cuantos ciclos del reloj transcurrieron
            CImpresion.Espera();

            List<CInventario> lista2 = new List<CInventario>();//Se crea una nueva lista
            reloj.Reset();
            reloj.Start();
            //AddRange en listas agrega una colección completa de elementos. 
            /*Despues del "from" se agrega una variable "o" que recibe el resultado de la consulta, de la lista "Productos"
             La condicion "where o.Existencias > 0 && o.PrecioPublico < 12" la existencia es menor que "0" y el PrecioPublico se menor que 12
             Al ultimo seleccionamos el nombre de la variable para tomar los datos "select o"*/
            lista.AddRange(from o in control.Productos where o.Existencias > 0 && o.PrecioPublico < 12 select o);
            reloj.Stop();
            CImpresion.Imprime(lista2);
            CImpresion.ImprimeTiempo(reloj.ElapsedTicks);
            CImpresion.Espera();
            //Agrupamos por costo 
            var lista3 = (from o in control.Productos group o by o.Costo into g select g).ToList();

        }
    }
}
