﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Condiciones
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int valor1 = 0, valor2 = 0;//Asigna 2 valores "Enteros"
            string cadena;//Lo utilizaremos para recibir el mensaje de consola
            Console.WriteLine("Ingresa el primer Numero: ");//Mostramos un mensaje
            cadena = Console.ReadLine();//Cadena sera lo que regtrese "WriteLine"
            valor1 = Convert.ToInt32(cadena);//Convertimos el valor cadena en "Entero"
            Console.WriteLine("Ingresa el segundo Numero: ");
            cadena = Console.ReadLine();
            valor2 = Convert.ToInt32(cadena);
            //If sirver para evaluar una condicion
            if (valor1 <= valor2)//Decimos que si el valor 1 es menor al valor 2
            { //Entonces
                if (valor1 == valor2)//Condicion para saber si el valor 1 es = al valor 2
                    Console.WriteLine("El valor 1 es igual al valor 2");
                else//Si la condicion no se cumple se pone "Else"
                    Console.WriteLine("El valor 2 es mayor o igual a el valor 1");
            }
            else
                Console.WriteLine("El valor 1 es mayor o igual a el valor 2");
            Console.ReadKey();
        }
    }
}
