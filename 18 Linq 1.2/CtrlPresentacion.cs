﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18_Linq_1._2
{
    public class CtrlPresentacion
    {
        public CtrlPresentacion()//CONSTRUCTOR
        {
            _Productos = new List<CInventario>();
        }
        private List<CInventario> _Productos;////Lista de CInventario 
        
        public List<CInventario> Productos//Propiedad "Productos" que instanciamos a _Productos
        {
            get {return _Productos; }
            set { _Productos = value ; }
        }

        public void Llenarlista()//METODO agrega la lista
        {
            Productos.Clear();
            Productos.Add(new CInventario()
            {
                IdProducto = 124,
                Clave = "P0001",
                Descripcion = "GTA CRAKETS 200g",
                PrecioPublico = 12.5m,
                Costo = 11.5m,
                Existencias = 0
            });
            Productos.Add(new CInventario()
            {
                IdProducto = 125,
                Clave = "P0022",
                Descripcion = "GTA MVILLAS 200g",
                PrecioPublico = 12.5m,
                Costo = 11.5m,
                Existencias = 3
            });
            Productos.Add(new CInventario()
            {
                IdProducto = 128,
                Clave = "P0034",
                Descripcion = "GTA MEXICANA 200g",
                PrecioPublico = 12.5m,
                Costo = 11.5m,
                Existencias = -4
            });
            Productos.Add(new CInventario()
            {
                IdProducto = 134,
                Clave = "P0045",
                Descripcion = "HIG LYS PAQ4PZAS",
                PrecioPublico = 23.5m,
                Costo = 18.5m,
                Existencias = 20
            });
        }    
    }
}
