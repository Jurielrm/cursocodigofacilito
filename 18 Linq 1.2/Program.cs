﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18_Linq_1._2
{
    class Program
    {
        static void Main(string[] args)
        {

            CtrlPresentacion control = new CtrlPresentacion();//Instanciamos
            control.Llenarlista();
            CImpresion.Imprime(control.Productos);//Imprime la lista "Productos"
            CImpresion.Espera();

            List<CInventario> lista = new List<CInventario>();//Se crea una nueva lista
            foreach (var item in control.Productos)//Recorre la lista Productos 
            {
                if (item.Existencias <= 0)//Checa el valor que tenga existencia menor a "0" 
                    lista.Add(item);//Se agregan a la lista todos los que tengan Existencia menor a "0"
            }
            CImpresion.Imprime(lista);//Imprime la nueva "lista"
            CImpresion.Espera();

            List<CInventario> lista2 = new List<CInventario>();//Se crea una nueva lista
            //AddRange en listas agrega una colección completa de elementos. 
            /*Despues del "from" se agrega una variable "o" que recibe el resultado de la consulta, de la lista "Productos"
             La condicion "where o.Existencias <= 0" Checa el valor que tenga existencia menor a "0"  
             Al ultimo seleccionamos el nombre de la variable para tomar los datos "select o"*/
            lista2.AddRange(from o in control.Productos where o.Existencias <= 0 select o);
            CImpresion.Imprime(lista2);
            CImpresion.Espera();
        }
    }
}
