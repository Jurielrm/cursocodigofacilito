﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18_Linq_1._2
{
    public static class CImpresion//La clase "static" permite utilizar la clase sin instanciarla  
    {
        public static void Imprime(List<CInventario> productos)
        {
            foreach (var producto in productos)
            {
                Console.WriteLine("{0} Codigo: {1} Descripcion: {2} Precio: {3} Costo: {4} Existencia {5}",
                                  producto.IdProducto, producto.Clave, producto.Descripcion, producto.PrecioPublico,
                                  producto.Costo, producto.Existencias);
            }
            Console.WriteLine("--------------------------------------------------------------------------------");
        }

        public static void Espera()
        {
            Console.ReadKey();
        }
    }
}
