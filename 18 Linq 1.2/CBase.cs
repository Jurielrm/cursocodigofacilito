﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18_Linq_1._2
{
    public class CBase : CAbs
    {
        public override int IdProducto { get; set; }
        
        public override string Clave { get; set; }
        
        public override string Descripcion { get; set; }
    }
}
