﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _18_Linq_1._2
{
    public class CPrecio : CBase
    {
        
        public decimal PrecioPublico { get; set; }

        public decimal Costo { get; set; }
        
    }
}
