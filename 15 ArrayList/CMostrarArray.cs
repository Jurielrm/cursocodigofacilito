﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_ArrayList
{
    public class CMostrarArray
    {
        ArrayList lista;

        public CMostrarArray()//Instanciamos
        {
            lista = new ArrayList();
        }

        public void Captura()//Metodo
        {
            string cadena;
                for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Ingrese el dato {0}: ",i + 1);
                cadena = Console.ReadLine();
                lista.Add(cadena);//Agrega
            }
        }

        public void Imprime()
        {
            foreach (var item in lista)//Buscamos 
            {
                Console.WriteLine(item);
            }
            Console.ReadKey();
        }
    }
}
