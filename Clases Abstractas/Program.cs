﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clases_Abstractas
{
    class Program
    {
        static void Main(string[] args)
        {
            //Podemos incluir los parametros de los constructores
            //Declaramos una variable cliente, la instanciamos directamente
            ClsClienteBase cliente = new ClsClienteBase(0,"Jaime Ramirez","001","JAM",1,"Jaime");

            Console.WriteLine(cliente.Clave + " " + cliente.Nombre);
            Console.WriteLine(cliente.TipoRegimen);
            Console.WriteLine(cliente.RFC);
            Console.ReadKey();
        }
    }
}
