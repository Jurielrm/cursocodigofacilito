﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiposVariablesyValores
{
    class Program
    {
        #region Método que requiere int 
        private string[] names = { "Spencer", "Sally", "Doug" };
        public string GetName(int ID)
        {
            if (ID < names.Length)
                return names[ID];
            else
                return String.Empty;
        }
        #endregion
        static void Main(string[] args)
        {
            #region Variable []
            /*El compilador utiliza información de tipos para asegurarse de que todas las operaciones 
             que se realizan en el código cumplen la seguridad de tipos. 
             Por ejemplo, si declara una variable de tipo int, 
             el compilador permite utilizar la variable en operaciones de suma y resta. 
             Si intenta realizar esas mismas operaciones con una variable de tipo bool, el compilador genera un error, 
             como se muestra en el ejemplo siguiente:*/
            int a = 5;
            int b = a + 2;

            bool test = true;
            //El operador '+' no se puede aplicar a operadore de tipo 'int' y bool
            //int c = a + test;
            #endregion 
            #region Declara Variables
            /*Al declarar una variable o una constante en un programa, 
            debe especificar su tipo o utilizar la palabra clave var para permitir que el compilador infiera el tipo.
            En el ejemplo siguiente se muestran algunas declaraciones de variables que utilizan tipos numéricos 
            integrados y tipos complejos definidos por el usuario:*/
            //Solo declaracion
            float temperatura;
            string nombre;
            MyClass myClass;
            //Declaraciones con inicializadores(por ejemplo)
            char primeraletra = 'C';
            var limite = 3;
            int[] source = { 0, 1, 2, 3, 4, 5 };
            var query = from item in source
                        where item <= limite
                        select item;
            #endregion
        }


    }
}
