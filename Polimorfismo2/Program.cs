﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo2
{
    class Program
    {
        /*Primero, cree una clase base llamada Shape y clases derivadas como Rectangle, Circle y Triangle.
        Dé a la clase Shape un método virtual llamado Draw e invalídelo en cada clase derivada 
        para dibujar la forma determinada que la clase representa.
        Cree un objeto List<Shape> y agregue Circle, Triangle y Rectangle a él.
        Para actualizar la superficie de dibujo, use un bucle foreach para iterar por la lista 
        y llamar al método Draw en cada objeto Shape de la lista.
        Aunque cada objeto de la lista tenga un tipo declarado de Shape, se invocará el tipo en tiempo de ejecución 
        (la versión invalidada del método en cada clase derivada).*/

        static void Main(string[] args)
        {
            /*Polimorfismo en el trabajo # 1: un rectángulo, un triángulo y un círculo
            se pueden usar todos donde se espera una Forma. 
            Ningún elenco es requerido porque existe una conversión implícita de un derivado
            clase a su clase base.*/
            System.Collections.Generic.List<Shape> shapes = new System.Collections.Generic.List<Shape>();
            shapes.Add(new Rectangulo());
            shapes.Add(new Circulo());
            shapes.Add(new Triangulo());

            /*Polimorfismo en el trabajo # 2: el método virtual Draw es
            invocado en cada una de las clases derivadas, no en la clase base*/
            foreach (Shape s in shapes)
            {
                s.Draw();
            }
            //Mantener la consola abierta en modo de depuración.
            Console.WriteLine("Presiona cualquier tecla para salir");
            Console.ReadKey();
        }
    }
}
