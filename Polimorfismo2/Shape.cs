﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo2
{
    class Shape
    {
        //Algunos miembros de ejemplo.
        public int X { get; private set; }
        public int Y { get; private set; }
        public int Height { get; set; }
        public int Width { get; set; }

        //Metodo Virtual
        public virtual void Draw()
        {
            Console.WriteLine("Realización de tareas de dibujo de clase base.");
        }
    }
}
