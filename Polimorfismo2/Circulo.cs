﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo2
{
    class Circulo : Shape
    {
        //Metodo Override "Anular"
        public override void Draw()
        {
            //Código para dibujar un círculo...
            Console.WriteLine("Dibujar un Circulo");
            base.Draw();
        }
        
    }
}
