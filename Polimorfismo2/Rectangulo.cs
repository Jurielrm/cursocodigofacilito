﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo2
{
    class Rectangulo : Shape
    {
        //Metodo Override "Anular"
        public override void Draw()
        {
            //Código para dibujar un rectangulo...
            Console.WriteLine("Dibujar un Rectangulo");
            base.Draw();
        }
    }
}
