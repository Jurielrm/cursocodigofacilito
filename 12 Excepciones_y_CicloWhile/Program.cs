﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            CJuegoRequisitos juego = new CJuegoRequisitos(0, "Assassins Creed", "OR541ASS", 1, "PS4", "Windows 10", "CPU i5 a 1,7 GHz", "4096 MB (4 GB)", "9.1");

            ISalida Imprime;
            string cadena = Console.ReadLine();
            if (cadena == "1")
                Imprime = new CImprime();
            else
                Imprime = new CArchivo();
            try
            {
                Imprime.ImprimeJuego(juego);
            }
            catch(Exception e)
            {
                Console.WriteLine("Ocurrio un error: " + e.Message);
                Console.ReadKey();
            }
        }
    }
}
