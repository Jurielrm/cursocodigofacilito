﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    public abstract class CAbsJuego
    {
        public abstract int IdJuego { get; set; }
        public abstract string Nombre { get; set; }
        public abstract string Clave { get; set; }
        public abstract int Clasificacion { get; set; }
        public abstract string Consola { get; set; }
    }
}
