﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    public class CJuegoBase : CAbsJuego
    {
        public CJuegoBase()
        {
            IdJuego = 0;
            Nombre = string.Empty;
            Clave = string.Empty;
            Clasificacion = 0;
            Consola = string.Empty;
        }

        public CJuegoBase(int pId, string pNombre, string pClave, int pClasificacion, string pConsola)
        {
            IdJuego = pId;
            Nombre = pNombre;
            Clave = pClave;
            Clasificacion = pClasificacion;
            Consola = pConsola;
        }
               
        public override int IdJuego { get; set; }
        public override string Nombre { get; set; }
        public override string Clave { get; set; }
        public override int Clasificacion { get; set; }
        public override string Consola { get; set; }
    }
}
