﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    public class CRequisitos
    {
        public CRequisitos()
        {
            SistemaOperativo = string.Empty;
            Procesador = string.Empty;
            Memoria = string.Empty;
            VersionDirectX = string.Empty;
        }

        public string SistemaOperativo { get; set; }
        public string Procesador { get; set; }
        public string Memoria { get; set; }
        public string VersionDirectX { get; set; }
    }
}
