﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    public class CJuegoRequisitos : CJuegoBase
    {
        public CJuegoRequisitos()
        {
            _Requisitos = new CRequisitos();
        }

        public CJuegoRequisitos (int jId, string jNombre, string jClave, int jClasificacion, string jConsola,string jSistema, string jProcesador, string jMemoria, string VersionX)
        {
            IdJuego = jId;
            Nombre = jNombre;
            Clave = jClave;
            Clasificacion = jClasificacion;
            Consola = jConsola;
            _Requisitos = new CRequisitos();
            Requisitos.SistemaOperativo = jSistema;
            Requisitos.Procesador = jProcesador;
            Requisitos.Memoria = jMemoria;
            Requisitos.VersionDirectX = VersionX;
        }

        private CRequisitos _Requisitos;
        public CRequisitos Requisitos
        {
            get {return _Requisitos ; }
            set { _Requisitos = value; }
        }
    }
}
