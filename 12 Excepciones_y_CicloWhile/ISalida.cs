﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    public interface ISalida
    {
        void ImprimeJuego(CAbsJuego juego);

        void ImprimeRequisitos(CRequisitos requisito);
       
    }
}
