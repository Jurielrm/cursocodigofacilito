﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    public class CImprime : ISalida
    {
        public void ImprimeJuego(CAbsJuego juego)
        {
            Console.WriteLine("CLAVE: " + juego.Clave + " " + "Nombre del juego: " + juego.Nombre);
            if(juego.Clasificacion == 1)
                Console.WriteLine("Tipo: CLASIFICACION T");
            else
                Console.WriteLine("Tipo: CLASIFICACION E");
            Console.WriteLine("Tipo de Consola: " + juego.Consola);
            Console.ReadKey();
        }

        public void ImprimeRequisitos(CRequisitos Requisito)
        {
            Console.WriteLine(Requisito.SistemaOperativo);
            Console.WriteLine(Requisito.Procesador);
            Console.WriteLine(Requisito.Memoria);
            Console.WriteLine(Requisito.VersionDirectX);
        }
    }
}
