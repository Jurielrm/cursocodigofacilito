﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Excepciones_y_CicloWhile
{
    public class CArchivo : ISalida
    {
        public void ImprimeJuego(CAbsJuego juego)
        {
            int i = 0;//Contador que cuenta cuantas veces se genera, y despues de un limite, generarlo 
            bool stop = true;   //Controlamos el ciclo infinito con una variable de tipo booleana
            //Nos permite hacer varias instrucciones repetitivas. Todas las instrucciones que esten dentro del "while"
            while (stop)//While se vuelve un infinito porque tiene la condicion "true"
            { 
                try//Para controlar las Excepciones utilizamos la instruccion "try"
                {
                    StreamWriter AC = new StreamWriter(@"C:\Users\CODE\Documents\Archivos.log", true);
                    AC.WriteLine("Clave: " + juego.Clave + " " + "Nombre del juego: " + juego.Nombre);
                    if (juego.Clasificacion == 1)
                        AC.WriteLine("Tipo: CLASIFICACION T");
                    else
                        AC.WriteLine("Tipo: CLASIFICACION E");
                    AC.WriteLine("Tipo de Consola: " + juego.Consola);
                    AC.Close();
                    stop = false;//Encaso de ningun error, entonces asi puede salir del ciclo
                }
                /*Agregamos la instruccion "catch" para capturar esa excepcion, como parametro ponemos un objeto "e" de tipo exception*/
                catch (Exception e)
                {
                    i++;
                    if(i>=1000)
                        throw e;//Se transfiere la excepecion del objeto "e", para que haga el manejo de la excepcion
                }
            }
        }

        public void ImprimeRequisitos(CRequisitos requisito)
        {
            StreamWriter AC = new StreamWriter(@"C:\Users\CODE\Documents\Archivos.log", true);
            AC.Close();
        }
    }
}
