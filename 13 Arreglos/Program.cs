﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _13_Arreglos
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creamos un arreglo de tipo entero, numero de elementos que tiene el arreglo "8" es la cantidad de ciclos que se van a dar
            int[] Arreglo = new int[8];
            //Ciclo "for" ejecuta varias instrucciones
            /*Se define una variable "int i", que maneja el indice del arreglo.
             "0" numero en que inicia.
             Si "i" es "<" menor a la "8", se va generar en 8 veces.
             "i++" va incrementa del 0 al 8*/
            int i;//Se declarar la variable adentro del "for"
            string cadena;
            for (i = 0; i < 8; i++)
            {
                //Arreglo[i] = 12;//Le asigna cualquier numero que tenga "i". 
                //Arreglo[i] = i + 1;//Inicia desde el numero 1.
                cadena = Console.ReadLine();//Repite los numeros ingresados
                Arreglo[i] = Convert.ToInt32(cadena);//Convierte cadena a entero
            }
            for (i = 0; i < 8; i++)
            {
                Console.WriteLine("{0}", Arreglo[i]);
                //Console.ReadKey();//"ReadKey" Parece los valore dando enter
            }
            Console.ReadKey();//"ReadKey" aparece los valores al ejecutarse el programa 
        }
    }
}
