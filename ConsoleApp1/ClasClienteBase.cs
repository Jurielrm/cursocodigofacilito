﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class ClasClienteBase : ClasAbsCliente
    {
        public ClasClienteBase ()
        {
            //Los CONSTRUCTORES llevan los valores de las PROPIEDADES 
            IdCliente = 0;
            Nombre = string.Empty;
            Clave = string.Empty;
            RFC = string.Empty;
            TipoRegimen = 0;
            NombreContacto = string.Empty;
        }

        public ClasClienteBase(int pId, string pNombre, string pClave, string pRFC, int pTipoRegimen, string pNombreContacto)
        {
            //Inicializa las PROPIEDADES de los PARAMETROS
            IdCliente = pId;
            Nombre = pNombre;
            Clave = pClave;
            RFC = pRFC;
            TipoRegimen = pTipoRegimen;
            NombreContacto = pNombreContacto;
        }

        //PROPIEDADES
        //La clase lleva el identificador override para indicar que esta implementando las propiedades de la clase ClsAbstClientes
        public override int IdCliente { get; set; }
        public override string Nombre { get; set; }
        public override string Clave { get; set; }
        public override string RFC { get; set; }
        public override int TipoRegimen { get; set; }
        public override string NombreContacto { get; set; }
    }
}
