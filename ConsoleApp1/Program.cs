﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            ClasClienteConContacto cliente = new ClasClienteConContacto(0, "Jaime Ramirez", "001", "JAM", 1, "Jaime", "282654258", "", "juriel@hotmail.com",
                                                                           " 1 de mayo", "45", "", "centro", "xalapa", "xalapa", "98562");
            
            interSalida Impresion;//Declaro una variable Impresion de la clase Interfaz
            string cadena = Console.ReadLine();//Definie una cadena que lea de la consola una linea
            if (cadena == "1")//Si cadena es igual a 1
                Impresion = new ClasImpresion();//Instanciamos Impresion como una consola
            else//Y si no
                Impresion = new ClasArchivos();//Instanciamos Impresion como una clase Archivo
            Impresion.ImprimeCliente(cliente);//Mandamos a llamar Impresion del metodo ImprimeCLiente y mandamos el objeto cliente
        }
    }
}
