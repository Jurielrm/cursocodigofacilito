﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public abstract class  ClasAbsCliente
    {
        //Agregar propiedad escribiendo prop y doble TAB
        //Para que la clase sea Abstracta de llevar el identificador abstract
        public abstract int IdCliente { get; set; }
        public abstract string Nombre { get; set; }
        public abstract string Clave { get; set; }
        public abstract string RFC { get; set; }
        public abstract int TipoRegimen { get; set; }
        public abstract string NombreContacto { get; set; }
    }
}
