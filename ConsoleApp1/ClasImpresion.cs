﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class ClasImpresion : interSalida
    {
        public void ImprimeCliente(ClasAbsCliente cliente)
        {
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(cliente.Clave + "" + cliente.Nombre);
            if (cliente.TipoRegimen == 1)
                Console.WriteLine("Tipo: PERSONA FISICA");
            else
                Console.WriteLine("Tipo: PERSONA MORAL");
            Console.WriteLine("RFC: " + cliente.RFC);
            Console.ReadKey();
        }

        public void ImprimeDireccion(ClasDirecciones Direccion)
        {
            Console.WriteLine(Direccion.Calle + " NO " + Direccion.NumeroExterior);
            Console.WriteLine(Direccion.Colonia);
            Console.WriteLine(Direccion.CP);
            Console.WriteLine(Direccion.Estado);
        }
    }
}
