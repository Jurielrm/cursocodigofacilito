﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{   //INTERFAZ
    public interface interSalida
    {
        //Declaramos la interfce Void
        //Metodo que Imprimecliente que recibe ClasAbsCliente 
        void ImprimeCliente(ClasAbsCliente cliente);

        void ImprimeDireccion(ClasDirecciones direccion);
    }
}
