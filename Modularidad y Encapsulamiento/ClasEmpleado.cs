﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modularidad_y_Encapsulamiento
{
    class ClasEmpleado
    {
        //CONSTRUCTOR
        public ClasEmpleado()
        {
            Nombre = "";
            SueldoDiario = 0.0m;
            Edad = 0;
        }
        
        private string _Nombre;//Variable privada
        //Tiene 2 metodos de acceso
        public string Nombre
        {
            //Obtiene - Regresa el valor de la variable privada
            get { return _Nombre; }
            //Agrega - Un valor a la variable privada
            set { _Nombre = value; }
        }

        //PROPIEDAD DIFERENTE A LA FORMA (ENCAPSULAMIENTO)
        //public decimal SueldoDiario;
        public decimal SueldoDiario { get; set; }
        
        //public int Edad;
        public int Edad { get; set; }
        
        public decimal CalculaSalario(int NumeroDias)
        {
            return SueldoDiario * NumeroDias;
        }
    }
}
