﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{   //"HERENCIA" La clase ClsClientesVentas ereda otra clase llamada ClsCliente  
    public class ClsClientesVentas : ClsCliente
    {
        private string _RFC;//Variable privada
        //Tiene 2 metodos de acceso
        public string RFC
        {
            get{ return _RFC; }//Obtiene - Regresa el valor de la variable privada
            set { _RFC = value; }//Agrega - Un valor a la variable privada
        }
        private string _Direccion;
        public string Direccion
        {
            get { return _Direccion; }
            set { _Direccion = value; }
        }    
        private string _Colonia;
        public string Colonia
        {
            get { return _Colonia; }
            set { _Colonia = value; }
        }
        private string _Municipio;
        public string Municipio
        {
            get { return _Municipio; }
            set { _Municipio = value; }
        }
        private bool _EsCredito;
        public bool EsCredito
        {
            get { return _EsCredito; }
            set { _EsCredito = value; }
        }
    }
}
