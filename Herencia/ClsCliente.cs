﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    public class ClsCliente
    {
        private int _IdCliente;//Variable privada
        //Tiene 2 metodos de acceso
        public int IdCliente
        {//ENCAPSULAMIENTO
            get{ return _IdCliente; }//Obtiene - Regresa el valor de la variable privada
            set{ _IdCliente = value; }//Agrega - Un valor a la variable privada
        }
        private string _Nombre;
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Apellidos;
        public string Apellidos
        {
            get { return _Apellidos; }
            set { _Apellidos = value; }
        }
    }
}
