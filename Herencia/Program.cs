﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            ClsClientesVentas Cliente;//Declaro una variable empleado de la clase ClsEmpleado
            Cliente = new ClsClientesVentas();//Instanciamos la clase empleado con new y despues el nombre del "CONSTRUCTOR"
            //En este apartado ya podemos utilizar el objeto empleado y las propiedades que tiene el CONSTRUCTOR
            Cliente.IdCliente = 123;
            Cliente.Apellidos = "Ramirez Martinez";
            Cliente.Nombre = "Jaime";
            Cliente.RFC = "RAM";
            Cliente.Colonia = "5 de mayo";
            Cliente.Direccion = "Siempre viva";
            Cliente.Municipio = "Xalapa";
            Cliente.EsCredito = false;
            Console.WriteLine(Cliente.Apellidos + " " + Cliente.Nombre);//Imprime los apellidos y el nombre
            Console.WriteLine(Cliente.Direccion + " " + Cliente.Colonia + " " + Cliente.Municipio);//Imprime la direccion, colonia y el municipio
            Console.WriteLine(Cliente.RFC);//Imprime el RFC
            //Si el cliente en la propiedad tiene TRUE, imprimira el mensaje que tiene credito
            if (Cliente.EsCredito)
                Console.WriteLine("El cliente tiene credito");
            else//Si tiene False imprimira que no tiene credito
                Console.WriteLine("El cliente no tiene credito");
            Console.ReadKey();
        }
    }
}
