﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _17_Genericos
{
    public class CClienteBase : CAbsCliente
    {
        public CClienteBase()
        {
            Id = 0;
            Nombre = string.Empty;
            Clave = string.Empty;
            RFC = string.Empty;
            TipoRegimen = 0;
            NombreContacto = string.Empty;
        }

        public CClienteBase(int pid, string pnombre, string pclave, string prfc, int pregimen, string pcontacto)
        {
            Id = pid;
            Nombre = pnombre;
            Clave = pclave;
            RFC = prfc;
            TipoRegimen = pregimen;
            NombreContacto = pcontacto;
        }

        public override int Id { get; set; }
        public override string Nombre { get; set; }
        public override string Clave { get; set; }
        public override string RFC { get; set; }
        public override int TipoRegimen { get; set; }
        public override string NombreContacto { get; set; }
    }
}
