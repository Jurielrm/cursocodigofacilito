﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _17_Genericos
{
    class Program
    {
        static void Main(string[] args)
        {
            string Opcion = "";
            List<CClienteBase> clientes; 
            clientes = new List<CClienteBase>();
            while(Opcion != "S")//Al precionar S cierra
            {
                Console.WriteLine("OPCIONES");
                Console.WriteLine("A. Agregar a la lista");
                Console.WriteLine("B. Quitar de la lista");
                Console.WriteLine("C. Imprimir lista");
                Console.WriteLine();
                Console.WriteLine("S. Salir");
                Opcion = Console.ReadLine();
                if (Opcion == "A")//Si se elije la opcion "A"
                {
                    string cadena = Console.ReadLine();
                    CClienteBase cliente = new CClienteBase();//Lee un valor
                    cliente.Id = clientes.Count + 1;//En lista la al agregar
                    cliente.Nombre = cadena;
                    clientes.Add(cliente);//Añade a la lista clientes
                }
                else if (Opcion == "B")//Si se elije la opcion "B"
                {
                    string cadena = Console.ReadLine();//Lee un valor
                    clientes.RemoveAt(Convert.ToInt32(cadena));
                }
                else if (Opcion == "C")
                {
                    foreach (CClienteBase item in clientes)
                    {
                        Console.WriteLine("Los datos del cliente son: ");
                        Console.WriteLine(item.Id.ToString() + " " + item.Nombre);//Muestra ID Y NOMBRE
                    }
                }
            }
        }
    }
}
