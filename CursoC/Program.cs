﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoC
{
    class Program
    {
        static void Main(string[] args)
        {
            //DECLARAR VARIABLE
            int TipoEntero = 0;
            decimal TipoMoneda = 0.0m;
            float TipoFlotante = 0.0f;
            double d = 0.0d;
            string Cadena = "Que tal";
            bool AdmiteDosPosiblesValores = false;
            DateTime fecha = DateTime.MinValue;
            Console.WriteLine("Tipo Entero: {0}", TipoEntero);
            Console.WriteLine("Tipo Moneda: {0:C}", TipoMoneda);
            Console.WriteLine("Un numero flotante: {0:F2}", TipoFlotante);
            Console.WriteLine("El valor de d: {0:F2}", d);
            Console.WriteLine("Un texto: " + Cadena);
            Console.WriteLine("Admite Dos Posibles Valores Verdadero O Falso: " + AdmiteDosPosiblesValores.ToString());
            Console.WriteLine("Fecha U Hora: " +fecha.ToShortDateString());
            Console.ReadKey();
        }
    }
}
