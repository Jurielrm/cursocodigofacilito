﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14_Arreglos_Multidimensionales
{
    public class CMatrices
    {
        private string[,] Matriz;//Declaramos Matriz de tipo entero, "," permite ingresar mas numeros

        public CMatrices()
        {
            Matriz = new string[4, 2];//Un arreglo, con "4, 2" elemntos. La Matriz tiene 4 filas y dos columnas
        }
        //Metodo 
        public void Inicia()
        {//Inicia
            for (int i = 0; i < 4; i++)//Ciclo "for" va depender de la dimension del arreglo
            {
                for (int j = 0; j < 2; j++)
                {
                    Matriz[i, j] = "hola";//Recorre los indices "i, j"
                }
            }
        }

        public void Imprime()
        {
            for (int i = 0; i< 4; i++)//Ciclo "for" va depender de la dimension del arreglo
            {
                for (int j = 0; j< 2; j++)
                {
               //Le quitamos Line    damos espacio
                    Console.Write("{0} ", Matriz[i, j]);//Imprime los valores
                }
                Console.WriteLine();//Ponemos WriteLine, los imprima en 2 columnas de 4 filas 
            }
            Console.ReadKey();
        }
    }
}
