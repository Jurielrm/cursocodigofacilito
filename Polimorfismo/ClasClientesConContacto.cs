﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    public class ClasClientesConContacto : ClasClienteBase
    {
        //CONSTRUCTOR Instanciamos Direccion
        public ClasClientesConContacto()
        {
            _Direcciones = new ClasDirecciones();
        }

        public ClasClientesConContacto(int pId, string pNombre, string pClave, string pRFC, int pTipoRegimen, string pNombreContacto,
                                       string pTelefono1, string pTelefono2, string pEmail, string pCalle, string pNumeroExterior,
                                       string pNumeroInterior, string pColonia, string pMunicipio, string pEstado, string pCP)
        {
            //Inicializa las PROPIEDAES de los PARAMETROS
            Id = pId;
            Nombre = pNombre;
            Clave = pClave;
            RFC = pRFC;
            TipoRegimen = pTipoRegimen;
            NombreContacto = pNombreContacto;
            Telefono1 = pTelefono1;
            Telefono2 = pTelefono2;
            Email = pEmail;
            _Direcciones = new ClasDirecciones();//Instanciamos Direccion para trabajar con el
            Direcciones.Calle = pCalle;
            Direcciones.NumeroExterior = pNumeroExterior;
            Direcciones.NumeroInterior = pNumeroInterior;
            Direcciones.Colonia = pColonia;
            Direcciones.Municipio = pMunicipio;
            Direcciones.Estado = pEstado;
            Direcciones.CP = pCP;
        }

        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Email { get; set; }
        //METODO Direcciones
        private ClasDirecciones _Direcciones;
        public ClasDirecciones Direcciones
        {
            get { return _Direcciones; }
            set {_Direcciones = value; }
        }
    }
}
