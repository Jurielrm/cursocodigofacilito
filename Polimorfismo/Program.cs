﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Podemos incluir los parametros de los constructores
            //Declaramos una variable cliente, la instanciamos directamente
            ClasClientesConContacto cliente = new ClasClientesConContacto (0, "Jaime Ramirez", "001", "JAM", 1, "Jaime", "282654258", "", "juriel@hotmail.com", 
                                                                           " 1 de mayo", "45", "", "centro", "xalapa", "xalapa", "98562");
            ClasImpresion Impresion;
            Impresion = new ClasImpresion();
            Impresion.ImprimirCliente(cliente);
        }
       
    }
}
