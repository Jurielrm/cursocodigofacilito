﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polimorfismo
{
    public class ClasClienteBase : ClasAbsCliente
    {
        //CONSTRUCTORES son metodos con el mismo nombre de la clase
        //"HERENCIA" La clase ClsClienteBase ereda otra clase llamada ClsAbstClientes
        public ClasClienteBase()
        {
            //Los constructores llevan los valores de las propiedades 
            Id = 0;
            Nombre = string.Empty;
            Clave = string.Empty;
            RFC = string.Empty;
            TipoRegimen = 0;
            NombreContacto = string.Empty;
        }
        //Este constructor se diferencia por sus parametros
        //Recibe los parametros... Se le agrega la "p" que sera la variable del parametro
        public ClasClienteBase(int pId, string pNombre, string pClave, string pRFC, int pTipoRegimen, string pNombreContacto)
        {
            //Inicializa las PROPIEDADES de los PARAMETROS
            Id = pId;
            Nombre = pNombre;
            Clave = pClave;
            RFC = pRFC;
            TipoRegimen = pTipoRegimen;
            NombreContacto = pNombreContacto;
        }
        //PROPIEDADES
        //La clase lleva el identificador override para indicar que esta implementando las propiedades de la clase ClsAbstClientes    
        public override int Id { get; set; }
        public override string Nombre { get; set; }
        public override string Clave { get; set; }
        public override string RFC { get; set; }
        public override int TipoRegimen { get; set; }
        public override string NombreContacto { get; set; }
    }
}

