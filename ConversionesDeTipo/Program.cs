﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConversionesDeTipo
{
    class Program
    {
        static void Main(string[] args)
        {
            int iEntero = 10;
            decimal xMoneda = 12.2m;
            bool bandera = false;
            //"Empty" sirve para poner una cadena vacia
            string cadenavacia = string.Empty;
            DateTime fecha = DateTime.MinValue;
            //CONVERSION MISMO VALOR "MANERA EXPLICITA"
            //xMoneda = iEntero;//Moneda tenga los 10 de que tiene Entero
            //CONVERSION DECIMAL A INT "Que un decimal sea un entero"
            //iEntero = (int)xMoneda;
            //CONVERTIR DE DECIMAL A UN ENTERO//Siendo que Entero tendra el mismo valor de Decimal 
            iEntero = Convert.ToInt32(xMoneda);
            Console.WriteLine("El valor de i es: {0}", iEntero);
            Console.WriteLine("El valor de d es: {0:C}", xMoneda);
            Console.WriteLine("El valor de bandera es: " + bandera.ToString());
            Console.WriteLine("El valor de cadena es: "+ cadenavacia);
            Console.WriteLine("El valor de fecha es: " + fecha.ToShortTimeString());
            Console.ReadKey();
        }
    }
}
